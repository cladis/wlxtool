/* 

 * id  | title   | monument id | uploader | upload date
 * int | varchar | int         | varchar  | timestamp
 */
/**
 * Author:  Base
 * Created: Sep 22, 2016
 */


DROP TABLE `wlx_ua_files`;

CREATE TABLE `wlx_ua_files`
(
`page_id` INT(20) NOT NULL,
`page_title` VARCHAR(1024) NOT NULL,
`monument_id` VARCHAR(32),
`uploader` VARCHAR(1024) NOT NULL,
`upload_date` TIMESTAMP NOT NULL,
`pixels` INT,
`sha` VARCHAR(128),
`year` INT,
`x` VARCHAR(5),
 PRIMARY KEY (`page_id`)
);


CREATE INDEX `id` ON `wlx_ua_files`(`page_id`);
CREATE INDEX `title` ON `wlx_ua_files`(`page_title`);
CREATE INDEX `monid` ON `wlx_ua_files`(`monument_id`);
CREATE INDEX `loader` ON `wlx_ua_files`(`uploader`);
CREATE INDEX `date` ON `wlx_ua_files`(`upload_date`);
CREATE INDEX `px` ON `wlx_ua_files`(`pixels`);
CREATE INDEX `sha` ON `wlx_ua_files` (`sha`);
CREATE INDEX `year` ON `wlx_ua_files` (`year`);
CREATE INDEX `x` ON `wlx_ua_files` (`x`);
