<?php

$ts_mycnf = parse_ini_file("/data/project/basebot/replica.my.cnf");

$localservername = "tools.labsdb";
$commonsservername = "commonswiki.labsdb";
$username = $ts_mycnf['user'];
$password = $ts_mycnf['password'];
$localdb = "{$username}__wlx_p";
$commonsdb = "commonswiki_p";
$heritagedb = "s51138__heritage_p";

// Create connection
$localconn = new mysqli($localservername, $username, $password, $localdb);
// Check connection
if ($localconn->connect_error) {
    die("Connection to user DB failed: " . $localconn->connect_error);
}
//echo 'connected to user DB at the least<br/>';

$ids = preg_split("/\|/", $_REQUEST["ids"]);
//var_dump($ids);
$ids_string = "(\n\"";
$first = true;
foreach ($ids as $id) {
    $id = trim($localconn->real_escape_string($id));
    $ids_string.= $first ? $id : "\",\n\"$id";
    $first = false;
}
$ids_string.= "\"\n)\n";
$sql = ""
        . "SELECT monument_id, page_title, uploader, sha, year\n"
        . "FROM `wlx_ua_files`\n"
        . "WHERE `monument_id` IN $ids_string\n";
//. "-- ORDER BY year ASC, monument_id ASC";
//var_dump($sql);


$result = $localconn->query($sql);
//var_dump($result);
echo $localconn->error;

$sqlheritage = ""
        . "SELECT id, name\n"
        . "FROM {$heritagedb}.monuments_all\n"
        . "WHERE country = 'ua'\n"
        . "AND id IN $ids_string\n";
$localconn->set_charset("utf8");
$heritageresult = $localconn->query($sqlheritage);
echo $localconn->error;

$localconn->close(); // we don't need the connection itself anymore

/*
 * I need to output all files
 * - for given ID
 * - for given IDs
 * - for IDs from given Monument list
 * - for given KOATUU
 * - for given city/raion/oblast/etc
 * 
 * - for given author
 * 
 *     |       ID      |
 *     |---------------|  PICTURE
 *     | YEAR | AUTHOR | 
 * 
 * 
 */

$monument_names = array();
if ($heritageresult->num_rows > 0) {
    while ($row = $heritageresult->fetch_assoc()) {
        //var_dump($row);
        $monument_names[$row["id"]] = $row["name"];
    }
    unset($heritagerow);
}

if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {
        $name = $row["page_title"];
        $name_urlencoded = urlencode($name);
        $md = str_split(md5($name));
        $src_start = "https://upload.wikimedia.org/wikipedia/commons";
        $src_hash = "{$md[0]}/{$md[0]}{$md[1]}";
        $size = 250;


        //https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/DSC00698-Антоновича%2C3.jpg/450px-DSC00698-Антоновича%2C3.jpg
        $src = $src_start;
        if (strlen($name) > 150) {
            $src .= "/$src_hash/$name_urlencoded";
        } else {
            $src .= "/thumb/$src_hash/$name_urlencoded/{$size}px-$name_urlencoded";
        }
        $name_htmlencoded = htmlspecialchars($name, ENT_QUOTES);
        //var_dump($name);
        $commons = "https://commons.wikimedia.org/wiki/";
        $href = "{$commons}File:" . urlencode("$name");
        $userpage = "{$commons}User:" . urlencode($row["uploader"]);

        $monument_id = $row["monument_id"];
        $monument_name = isset($monument_names[$monument_id]) ? "<br/>"
                . "<span class='monument-name'>{$monument_names[$monument_id]}</span>" : "";
        echo "\t\t\t\t<tr class='mainrow'>\n";
        echo "\t\t\t\t\t<td colspan='2' class='identifier'>$monument_id$monument_name</td>\n";
        echo "\t\t\t\t\t<td rowspan='2'>\n";
        echo "\t\t\t\t\t\t<a href='$href'>";
        echo "<img src='$src' width='{$size}px' alt='File «{$name_htmlencoded}» from Wikimedia Commons'/>";
        echo "</a>\n";
        echo "\t\t\t\t</td>\n";
        echo "\t\t\t\t</tr>\n";
        echo "\t\t\t\t<tr>\n";
        echo "\t\t\t\t\t<td>{$row["year"]}</td>\n";
        echo "\t\t\t\t\t<td><a href='$userpage'>{$row["uploader"]}</a></td>\n";
        echo "\t\t\t\t</tr>\n";
    }
    unset($row);
}
