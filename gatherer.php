<?php

error_reporting(E_ALL);
/**
 * I've got to get list of files in given category,
 * for each of them read:
 * - monument id
 * - uploader
 * - upload date
 * populate db with that.
 * schema to be is as following:
 * id  | title   | monument id | uploader | upload date
 * int | varchar | int         | varchar  | timestamp
 * 
 */
$wiki = "commons.wikimedia.org";
$apibase = "$wiki/w/api.php";

$ts_mycnf = parse_ini_file("/data/project/basebot/replica.my.cnf");

$localservername = "tools.labsdb";
$commonsservername = "commonswiki.labsdb";
$username = $ts_mycnf['user'];
$password = $ts_mycnf['password'];
$localdb = "{$username}__wlx_p";
$commonsdb = "commonswiki_p";

// Create connection
$commonsconn = new mysqli($commonsservername, $username, $password, $commonsdb);
// Check connection
if ($commonsconn->connect_error) {
    die("Connection to Commons DB failed: " . $commonsconn->connect_error);
}
echo 'connected to Commons DB at the least<br/>';


$years = array(2012, 2013, 2014, 2015, 2016);
$files = array();

foreach ($years as $year) {
    $sql = "
SELECT page_id, page_title, rev_user_text, rev_timestamp, img_width * img_height as pixels, img_sha1 as sha
FROM commonswiki_p.categorylinks
LEFT JOIN commonswiki_p.page ON commonswiki_p.categorylinks.cl_from = commonswiki_p.page.page_id
LEFT JOIN commonswiki_p.revision ON commonswiki_p.categorylinks.cl_from = rev_page AND rev_parent_id = 0
LEFT JOIN commonswiki_p.image ON page_title = commonswiki_p.image.img_name
WHERE cl_to IN (
  \"Images_from_Wiki_Loves_Monuments_{$year}_in_Ukraine\"
)
AND page_namespace = 6
ORDER BY pixels ASC";

    $result = $commonsconn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["year"] = $year;
            $files[] = $row;
            unset($row);
        }
    }
}
$commonsconn->close(); // we don't need the connection itself anymore
unset($year); // we don't need it anymore and with such a name it might cause problems


$monument_ids = array();

$files_number = count($files);
echo "Found files: $files_number";

for ($i = 0; $i < $files_number; $i+=49) {

    $first = true;
    $ids = "";
    for ($j = $i; $j < count($files) && $j < $i + 49; $j++) {
        //var_dump($ids);

        $ids .=!$first ? "|" . $files[$j]["page_id"] : $files[$j]["page_id"];
        $first = false;
    }
    //echo $ids;
    $monument_ids += read_monument_ids($ids);
    unset($ids, $first);
    echo "$i\n";
}

// Create connection
$localconn = new mysqli($localservername, $username, $password, $localdb);
// Check connection
if ($localconn->connect_error) {
    die("Connection to user DB failed: " . $localconn->connect_error);
}
echo 'connected to user DB at the least<br/>';

foreach ($files as $file) {
    $insert = "\n"
            . "INSERT INTO `wlx_ua_files` VALUES(\n"
            . " {$localconn->real_escape_string($file["page_id"])},\n"
            . "\"{$localconn->real_escape_string($file["page_title"])}\",\n"
            . "\"{$localconn->real_escape_string($monument_ids[$file["page_id"]])}\",\n"
            . "\"{$localconn->real_escape_string($file["rev_user_text"])}\",\n"
            . "\"{$localconn->real_escape_string($file["rev_timestamp"])}\",\n"
            . "{$localconn->real_escape_string($file["pixels"])},"
            . "\"{$localconn->real_escape_string($file["sha"])}\",\n"
            . "{$localconn->real_escape_string($file["year"])},\n"
            . "\"wlm\"\n"
            . ")\n"
            . "ON DUPLICATE KEY UPDATE\n"
            . "`page_id` = {$localconn->real_escape_string($file["page_id"])},\n"
            . "`page_title` = '{$localconn->real_escape_string($file["page_title"])}',\n"
            . "`monument_id` = '{$localconn->real_escape_string($monument_ids[$file["page_id"]])}',\n"
            . "`uploader` = '{$localconn->real_escape_string($file["rev_user_text"])}',\n"
            . "`upload_date` = '{$localconn->real_escape_string($file["rev_timestamp"])}',\n"
            . "`pixels` = {$localconn->real_escape_string($file["pixels"])},\n"
            . "`sha` = '{$localconn->real_escape_string($file["sha"])}',\n"
            . "`year` = {$localconn->real_escape_string($file["year"])},\n"
            . "`x` = \"wlm\"\n"
            . ";\n";
    $localconn->query($insert);
    echo $localconn->error;

    unset($insert);
}
$localconn->close();
$now = date('Y-m-d H:i:s', time());
var_dump($now);
file_put_contents("/data/project/basebot/"/* /public_html/WLXtool/ */ . "lastdbupdate.txt", $now);
echo "fin";

function read_monument_ids($ids) {
    //echo "reading monument ids for pages: $ids";
    $monument_ids = array();
    //https://commons.wikimedia.org/w/api.php?action=query&format=xml&prop=revisions&rvprop=content&pageids=51238109|51351117|51349165
    $api_response = file_get_contents("https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&pageids=$ids");
    //var_dump($api_response);
    $decoded_response = json_decode($api_response);
    $pages = $decoded_response->query->pages;
    foreach ($pages as $page) {
        //var_dump($page);
        $revisions = $page->revisions;
        $page_id = $page->pageid;
        foreach ($revisions as $revision) {
            $star = "*";
            $content = $revision->$star;
            $regex = "/.*\{\{\s*[Mm]onument[ _]Ukraine\|\s*([^}]*)\s*}}.*/";
            $match = array();
            preg_match($regex, $content, $match);
            $monument_id = isset($match[1]) ? $match[1] : "NO TEMPLATE MATCH";
            unset($match, $content);
            //var_dump($monument_id);

            $monument_ids[$page_id] = $monument_id;
        }
    }
    return($monument_ids);
}

// TEST
