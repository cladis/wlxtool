<!DOCTYPE html>
<!--
Let it be under condition of CC BY-SA 4.0. untill I get some idea about licenses
more suitable for software. FOSS rules, heh.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Base no WLX turu</title>
        <link rel="stylesheet" type="text/css" href="styles/styles.css"/>
        <script type="text/javascript" src="js/libs/jquery/jquery.js"></script>
        <script type="text/javascript">
            // var rows_per_page = 50;
            function getPage()
            {
                //Clear the existing data view
                $('#photo-rows').html('');
                $('#photo-rows').append("<tr><td colspan='3'>LOADING</td></tr>");
                //Get subset of data
                var queryPart = "?ids=" + $('#monument-ids').val();
                $.get("rows.php" + queryPart, function (data) {
                    $('#photo-rows').html('');
                    $('#photo-rows').append(data);
                    document.location.search = queryPart;
                });
            }


            $(document).ready(function () {
                //getPage();
            });
        </script>
    </head>
    <body>
        <div class="header">
            Base no WLX turu    
        </div>
        <?php
        $time = file_get_contents("/data/project/basebot/"/* /public_html/WLXtool/ */ . "lastdbupdate.txt");
        echo "<p>Data as of $time (UTC)";
        ?>

        <form name="mainform" action="#" method="get">
            <table>
                <tr>
                    <td>
                        <label>Monument ID(s):
                            <input type="text" placeholder="00-000-0000|00-000-0001" name="ids" id="monument-ids" <?php
                            echo isset($_REQUEST["ids"]) ? "value=\"" . htmlspecialchars($_REQUEST["ids"], ENT_QUOTES) . "\"" : "";
                            ?> /><br/>
                            <span class="formattip">Use pipe as a delimiter for multiple values</span>
                        </label>  
                    </td>
                    <td>
                        <input type="button" onclick="getPage()" value="SHOW THE IMAGES" class="submitbutton"/>
                    </td>
                </tr>
            </table>
        </form>
        <table class="photos" id="photos-table">
            <thead>
                <tr>
                    <th colspan="2">Monument ID</th>
                    <th rowspan="2">Photo</th>
                </tr>
                <tr>
                    <th>Contest year</th>
                    <th>Uploader</th>
                </tr>
            </thead>
            <tbody id="photo-rows">
                <?php
                require_once 'rows.php';
                ?>
            </tbody>
        </table>
    </body>
</html>
